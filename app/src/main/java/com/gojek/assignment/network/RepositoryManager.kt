package com.gojek.assignment.network

import com.gojek.assignment.room.entity.Repository

interface RepositoryManager {

    fun fetchRepositories(callback: (List<Repository>) -> Unit)
}