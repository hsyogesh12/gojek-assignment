package com.gojek.assignment.network

import com.gojek.assignment.data.RemoteDataSource
import com.gojek.assignment.local.LocalRepositoryManager
import com.gojek.assignment.room.entity.Repository
import com.gojek.assignment.schedulers.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class RemoteRepositoryMangerImpl(
    private val remoteDataSource: RemoteDataSource,
    private val mSchedulerProvider:SchedulerProvider,
    private var compositeDisposable: CompositeDisposable,
    private val localRepositoryManager: LocalRepositoryManager
) : RepositoryManager {

    override fun fetchRepositories(callback: (List<Repository>) -> Unit) {

        val disposable = remoteDataSource.getRepositories()
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .subscribe({ this.handleResponse(it, callback) })
            // return empty list on call back on error
            {
                callback(emptyList())
            }
        compositeDisposable.add(disposable)

    }

    private fun handleResponse(
        repositories: List<Repository>,
        callback: (List<Repository>) -> Unit
    ) {
        localRepositoryManager.saveRepositoriesToCache(repositories) { callback(repositories) }
    }
}