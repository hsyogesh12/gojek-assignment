package com.gojek.assignment.room.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "builtBy")

data class BuiltBy(
    var username: String,
    var href: String,
    var avatar: String,
    var repoId: Long
)
{
    @PrimaryKey(autoGenerate = true)
    var id: Int?=null
}