package com.gojek.assignment.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "repository_items")
data class Repository(
    @ColumnInfo(name = "author") var author: String,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "avatar", defaultValue = "https://github.com/LingDong-.png") var avatar: String,
    @ColumnInfo(name = "url") var url: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "language", defaultValue = "C++") var language: String = "",
    @ColumnInfo(name = "languageColor", defaultValue = "#3572A5") var languageColor: String,
    @ColumnInfo(name = "stars") var stars: Int,
    @ColumnInfo(name = "forks") var forks: Int,
    @ColumnInfo(name = "currentPeriodStars") var currentPeriodStars: Int
) {
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    @Ignore
    var builtBy: List<BuiltBy>? = null
    @Ignore
    var expanded: Boolean = false
}
