package com.gojek.assignment.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.gojek.assignment.room.dao.BuiltByDao
import com.gojek.assignment.room.dao.RepositoryDao
import com.gojek.assignment.room.entity.BuiltBy
import com.gojek.assignment.room.entity.Repository

@Database(
    entities = [Repository::class, BuiltBy::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun repositoryDao(): RepositoryDao
    abstract fun builtByDao(): BuiltByDao
    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance
            ?: synchronized(LOCK){
            instance
                ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "repository-list.db")
            .build()

    }
}