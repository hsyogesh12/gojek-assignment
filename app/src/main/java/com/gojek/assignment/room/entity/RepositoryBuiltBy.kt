package com.gojek.assignment.room.entity

import androidx.room.Embedded
import androidx.room.Relation

data class RepositoryBuiltBy (

    @Embedded val repository: Repository,
    @Relation(
        parentColumn = "id",
        entityColumn = "repoId"
    )
    val builtBy: List<BuiltBy>

)
