package com.gojek.assignment.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gojek.assignment.room.entity.Repository

@Dao
interface RepositoryDao {

    @Query("SELECT * FROM repository_items")
    fun getAll(): List<Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insert(list: Repository) : Long

    @Query("DELETE FROM repository_items")
    fun deleteAll()
}