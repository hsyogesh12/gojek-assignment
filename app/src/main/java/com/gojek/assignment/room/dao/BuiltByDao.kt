package com.gojek.assignment.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.gojek.assignment.room.entity.BuiltBy

@Dao
interface BuiltByDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    fun insertAll(kist: List<BuiltBy>)

    @Query("DELETE FROM repository_items")
    fun deleteAll()
}