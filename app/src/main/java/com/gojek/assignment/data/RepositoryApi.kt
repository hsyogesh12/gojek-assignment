package com.gojek.assignment.data

import com.gojek.assignment.room.entity.Repository
import io.reactivex.Observable
import retrofit2.http.GET

interface RepositoryApi {

    @GET("/repositories?language=&since=daily")
    fun getRepositories() : Observable<List<Repository>>

}