package com.gojek.assignment.data

import com.gojek.assignment.room.entity.Repository

import io.reactivex.Observable
import retrofit2.Retrofit

/**
 * Created by javierg on 20/11/2017.
 */

class RemoteDataSource(retrofit: Retrofit) : RepositoryApi {

    private val api: RepositoryApi = retrofit.create(RepositoryApi::class.java)

    override fun getRepositories(): Observable<List<Repository>> {
        return api.getRepositories()
    }
}
