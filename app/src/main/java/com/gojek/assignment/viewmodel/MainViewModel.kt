package com.gojek.assignment.viewmodel

import androidx.lifecycle.ViewModel
import com.gojek.assignment.local.LocalRepositoryManager
import com.gojek.assignment.network.RepositoryManager
import com.gojek.assignment.room.entity.Repository
import com.gojek.assignment.utils.AppUtil


class MainViewModel constructor(
    private val remoteRepositoryManager: RepositoryManager,
    private val appUtil: AppUtil,
    private val localRepositoryManager: LocalRepositoryManager
) : ViewModel() {

    fun getRepositoriesFromRemote(callback: (List<Repository>) -> Unit) = remoteRepositoryManager.fetchRepositories(callback)

    fun getRepositories(callback: (List<Repository>) -> Unit) {
        if (appUtil.isCacheExpired()) {
            remoteRepositoryManager.fetchRepositories(callback)
        } else
            localRepositoryManager.fetchRepositories(callback)
    }

}