package com.gojek.assignment.local

import com.gojek.assignment.constants.ConstantPreference
import com.gojek.assignment.preferences.AppPreferences
import com.gojek.assignment.room.dao.BuiltByDao
import com.gojek.assignment.room.dao.RepositoryDao
import com.gojek.assignment.room.entity.Repository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class LocalRepositoryManagerImpl constructor(
    private val repositoryDao: RepositoryDao,
    private val builtByDao: BuiltByDao,
    private val appPreferences: AppPreferences
) : LocalRepositoryManager {


    override fun saveRepositoriesToCache(
        list: List<Repository>,
        callback: () -> Unit
    ) {
        if (list.isNotEmpty()) {
            GlobalScope.launch {
                repositoryDao.deleteAll()
                builtByDao.deleteAll()
                list.forEach() {
                    val id = repositoryDao.insert(it)
                    it.builtBy?.let { it.map { builtBy -> builtBy.repoId = id } }
                    it.builtBy?.let { builtByDao.insertAll(it) }
                }
                appPreferences[ConstantPreference.cacheExpiry] = Date().time
                callback()
            }
        }
        else
            callback()
    }

    override fun fetchRepositories(callback: (List<Repository>) -> Unit) {
        GlobalScope.launch {
            val list = repositoryDao.getAll()
            callback(list)
        }
    }
}