package com.gojek.assignment.local

import com.gojek.assignment.network.RepositoryManager
import com.gojek.assignment.room.entity.Repository

interface LocalRepositoryManager : RepositoryManager {

    fun saveRepositoriesToCache(
        list: List<Repository>,
        callback: () -> Unit
    )
}