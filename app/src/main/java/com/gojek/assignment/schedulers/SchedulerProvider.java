package com.gojek.assignment.schedulers;

import androidx.annotation.NonNull;
import io.reactivex.Scheduler;

public interface SchedulerProvider {

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();
}
