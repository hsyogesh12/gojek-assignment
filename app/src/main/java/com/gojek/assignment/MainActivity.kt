package com.gojek.assignment

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.gojek.assignment.adapter.RepositoryAdapter
import com.gojek.assignment.databinding.ActivityMainBinding
import com.gojek.assignment.room.entity.Repository
import com.gojek.assignment.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.no_connection_error.*
import kotlinx.android.synthetic.main.shimmer_layout.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val mainViewModel by viewModel<MainViewModel>()
    private var binding: ActivityMainBinding? = null
    private var repositoryList: List<Repository> = mutableListOf()
    private val adapter = RepositoryAdapter(mutableListOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initToolBar()
        initRecyclerView()

        fetchDataFromRepository()

        swipeContainer?.setOnRefreshListener {
            showLoadingView()
            mainViewModel.getRepositoriesFromRemote {
                repositoryList = it
                updateUi(it)
            }

        }

        retryButton?.setOnClickListener {
            showLoadingView()
            fetchDataFromRepository()
        }
    }

    private fun showLoadingView() {
        swipeContainer.visibility = View.GONE
        shimmer_layout.visibility = View.VISIBLE
        shimmer_view_container.startShimmerAnimation()
    }

    private fun initToolBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initRecyclerView() {
        (recyclerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
    }

    private fun fetchDataFromRepository() {
        mainViewModel.getRepositories {
            repositoryList = it
            runOnUiThread {
                updateUi(it)
            }
        }
    }

    private fun updateUi(
        it: List<Repository>
    ) {
        runOnUiThread {
            swipeContainer.isRefreshing = false
            shimmer_view_container.stopShimmerAnimation()
            if (it.isEmpty()) {
                no_network_view.visibility = View.VISIBLE
                swipeContainer.visibility = View.GONE
                shimmer_layout.visibility = View.GONE
            } else {
                no_network_view.visibility = View.GONE
                shimmer_layout.visibility = View.GONE
                swipeContainer.visibility = View.VISIBLE
                adapter.updateItems(it)
            }
        }
    }

    public override fun onResume() {
        super.onResume()
        shimmer_view_container.startShimmerAnimation()
    }

    override fun onPause() {
        shimmer_view_container.stopShimmerAnimation()
        super.onPause()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sortByName -> adapter.updateItems(repositoryList.sortedWith(compareBy { it.name }))
            R.id.sortByStars -> adapter.updateItems(repositoryList.sortedWith(compareBy { it.stars }))
        }
        return true
    }
}
