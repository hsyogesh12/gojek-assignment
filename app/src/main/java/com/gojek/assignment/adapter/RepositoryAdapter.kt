package com.gojek.assignment.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gojek.assignment.R
import com.gojek.assignment.databinding.RepoRowItemBinding
import com.gojek.assignment.room.entity.Repository

class RepositoryAdapter(private var repositoryList: List<Repository>) :
    RecyclerView.Adapter<RepositoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val rowItemBinding = DataBindingUtil.inflate<RepoRowItemBinding>(LayoutInflater
            .from(parent.context), R.layout.repo_row_item, parent, false)
        return ViewHolder(rowItemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repository = repositoryList[position]
        holder.bind(repository)
        holder.itemView.setOnClickListener {
            val expanded = repository.expanded
            repositoryList.forEach { it.expanded = false }
            repository.expanded = !expanded
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(private val binding: RepoRowItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Repository) {
            binding.repository = item
            binding.subItem.visibility = if(item.expanded) View.VISIBLE else View.GONE
            binding.executePendingBindings()
        }
    }

    fun updateItems(repositories: List<Repository>){
        this.repositoryList = repositories
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return repositoryList.size
    }
}