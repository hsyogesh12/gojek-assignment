package com.gojek.assignment.binders

import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

@BindingAdapter(value = ["imageIcon"], requireAll = false)
fun imageIcon(view: ImageView, imageUrl: String) {
    Glide.with(view)
        .load(imageUrl)
        .into(view)

}

@BindingAdapter(value = ["backgroundTint"], requireAll = false)
fun backgroundTint(view: AppCompatImageView, colorCode: String?) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        colorCode?.let { view.backgroundTintList = ColorStateList.valueOf(Color.parseColor(colorCode)) }
    }

}