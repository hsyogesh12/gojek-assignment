package com.gojek.assignment.di

import com.gojek.assignment.data.RemoteDataSource
import com.gojek.assignment.data.RepositoryApi
import com.gojek.assignment.network.RemoteRepositoryMangerImpl
import com.gojek.assignment.network.RepositoryManager
import com.gojek.assignment.schedulers.SchedulerProvider
import com.gojek.assignment.schedulers.SchedulerProviderImpl
import io.reactivex.disposables.CompositeDisposable
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val remoteRepositoryModule = module {

    single {
        RemoteDataSource(
            Retrofit.Builder().baseUrl("https://github-trending-api.now.sh")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
        )
    }
    single { CompositeDisposable() }
    single<RepositoryApi> { RemoteDataSource(get()) }
    single<SchedulerProvider> { SchedulerProviderImpl() }
    single<RepositoryManager> { RemoteRepositoryMangerImpl(get(), get(), get(), get()) }

}