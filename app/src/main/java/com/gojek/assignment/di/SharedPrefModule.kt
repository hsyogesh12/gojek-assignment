package com.gojek.assignment.di

import android.content.Context
import com.gojek.assignment.preferences.AppPreferences
import com.gojek.assignment.preferences.PreferenceHelper
import com.gojek.assignment.preferences.PreferenceHelper.Companion.SharedPreference
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val sharedPrefModule = module {
    single<AppPreferences> {
        PreferenceHelper(
            (androidContext().getSharedPreferences(
                SharedPreference, Context.MODE_PRIVATE
            ))
        )
    }
}