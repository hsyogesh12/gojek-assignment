package com.gojek.assignment.di

import com.gojek.assignment.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule =  module {
    viewModel { MainViewModel(get(), get(), get()) }
}