package com.gojek.assignment.di

import androidx.room.Room
import com.gojek.assignment.local.LocalRepositoryManager
import com.gojek.assignment.local.LocalRepositoryManagerImpl
import com.gojek.assignment.room.AppDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val localRepositoryModule = module {

    single {
        val appDatabase = Room.databaseBuilder(
            this.androidContext(),
            AppDatabase::class.java, "repository-list.db"
        ).build()
        appDatabase.repositoryDao()
    }

    single {
        val appDatabase = Room.databaseBuilder(
            this.androidContext(),
            AppDatabase::class.java, "repository-list.db"
        ).build()
        appDatabase.builtByDao()
    }

    single<LocalRepositoryManager> { LocalRepositoryManagerImpl(get(), get(), get()) }

}