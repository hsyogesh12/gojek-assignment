package com.gojek.assignment.di

import com.gojek.assignment.utils.AppUtil
import org.koin.dsl.module

val utilModule =  module {
    single { AppUtil(get()) }
}