package com.gojek.assignment.extensions

import com.gojek.assignment.GojekApplication
import com.gojek.assignment.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

fun GojekApplication.prepareDI() {

    startKoin {
        androidContext(this@prepareDI)
        modules(
            listOf(
                appModule,
                remoteRepositoryModule,
                localRepositoryModule,
                sharedPrefModule,
                utilModule
            )
        )
    }

}
