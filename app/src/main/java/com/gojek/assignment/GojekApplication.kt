package com.gojek.assignment

import android.app.Application
import com.facebook.stetho.Stetho
import com.gojek.assignment.extensions.prepareDI

class GojekApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        prepareDI()
    }
}