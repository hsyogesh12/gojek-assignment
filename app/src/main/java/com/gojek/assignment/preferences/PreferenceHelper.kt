package com.gojek.assignment.preferences

import android.content.SharedPreferences

class PreferenceHelper constructor(private val sharedPref: SharedPreferences) :
    AppPreferences {

  override fun getString(key: String, defaultValue: String): String? {
    return sharedPref.getString(key, defaultValue)
  }

  override fun getLong(key: String, defaultValue: Long): Long {
    return sharedPref.getLong(key, defaultValue)
  }

  override fun getInt(key: String, defaultValue: Int): Int {
    return sharedPref.getInt(key, defaultValue)
  }

  override fun getFloat(key: String, defaultValue: Float): Float {
    return sharedPref.getFloat(key, defaultValue)
  }

  override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
    return sharedPref.getBoolean(key, defaultValue)
  }

  private fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
    val editor = this.edit()
    operation(editor)
    editor.apply()
  }

  override operator fun set(key: String, value: Any?) {
    when (value) {
      is String -> sharedPref.edit { it.putString(key, value) }
      is Int -> sharedPref.edit { it.putInt(key, value) }
      is Boolean -> sharedPref.edit { it.putBoolean(key, value) }
      is Float -> sharedPref.edit { it.putFloat(key, value) }
      is Long -> sharedPref.edit { it.putLong(key, value) }
      else -> throw UnsupportedOperationException("Not yet implemented")
    }
  }

  companion object {
    const val SharedPreference = "gojek_preferences"
  }
}