package com.gojek.assignment.preferences

interface AppPreferences {

  fun getBoolean(key: String, defaultValue: Boolean = false): Boolean

  fun getString(key: String, defaultValue: String = ""): String?

  fun getLong(key: String, defaultValue: Long = -1): Long

  fun getInt(key: String, defaultValue: Int = -1): Int

  fun getFloat(key: String, defaultValue: Float = -1f): Float

  operator fun set(key: String, value: Any?)
}