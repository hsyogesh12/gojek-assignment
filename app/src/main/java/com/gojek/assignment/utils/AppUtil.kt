package com.gojek.assignment.utils

import com.gojek.assignment.constants.ConstantPreference
import com.gojek.assignment.preferences.AppPreferences
import java.util.*
import java.util.concurrent.TimeUnit

class AppUtil constructor(private val preferences: AppPreferences) {


    fun isCacheExpired() : Boolean {
        val timeInMillis = preferences.getLong(ConstantPreference.cacheExpiry, -1L)
        if(timeInMillis == (-1).toLong()) {
            return true
        }
        val cachedDate = Date(timeInMillis)
        val currentDate = Date()
        val milliseconds = currentDate.time - cachedDate.time
        val toHours = TimeUnit.MILLISECONDS.toHours(milliseconds)
        return toHours > 2

    }
}