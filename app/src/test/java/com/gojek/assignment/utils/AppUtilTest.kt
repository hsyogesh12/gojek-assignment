package com.gojek.assignment.utils

import com.gojek.assignment.constants.ConstantPreference
import com.gojek.assignment.preferences.AppPreferencesSpy
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.*
import java.util.Calendar.HOUR

@RunWith(MockitoJUnitRunner::class)
class AppUtilTest {

    private lateinit var appUtil: AppUtil
    private var appPreferencesSpy = AppPreferencesSpy()

    @Before
    fun setUp() {
        appUtil = AppUtil(appPreferencesSpy)
        appPreferencesSpy[ConstantPreference.cacheExpiry] = -1L
    }

    @Test
    fun testIsCacheExpired() {
        assertTrue(appUtil.isCacheExpired())
        appPreferencesSpy[ConstantPreference.cacheExpiry] = Date().time
        assertFalse(appUtil.isCacheExpired())
        setHoursDelayBy2Hours()
        assertTrue(appUtil.isCacheExpired())
    }

    private fun setHoursDelayBy2Hours() {
        val calender = Calendar.getInstance()
        calender.set(HOUR, calender.get(HOUR) - 3)
        appPreferencesSpy[ConstantPreference.cacheExpiry] = calender.timeInMillis
    }

}