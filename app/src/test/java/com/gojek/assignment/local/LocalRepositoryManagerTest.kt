package com.gojek.assignment.local

import com.gojek.assignment.constants.ConstantPreference
import com.gojek.assignment.preferences.AppPreferencesSpy
import com.gojek.assignment.room.dao.BuiltByDao
import com.gojek.assignment.room.dao.RepositoryDao
import com.gojek.assignment.room.entity.Repository
import com.google.gson.Gson
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class LocalRepositoryManagerTest {


    private var appPreferencesSpy = AppPreferencesSpy()
    private lateinit var localRepositoryManager: LocalRepositoryManager

    @Mock
    private lateinit var repositoryDao: RepositoryDao

    @Mock
    private lateinit var builtByDao: BuiltByDao

    @Before
    fun setUp() {
        localRepositoryManager =
            LocalRepositoryManagerImpl(repositoryDao, builtByDao, appPreferencesSpy)
    }

    @Test
    fun testSavingRepositoriesToCache() {
        val spyList = getSpyList()
        localRepositoryManager.saveRepositoriesToCache(spyList) {
            verify(repositoryDao).deleteAll()
            verify(builtByDao).deleteAll()
            verify(repositoryDao).insert(spyList[0])
            verify(builtByDao).insertAll(spyList[0].builtBy!!)
            assertEquals(appPreferencesSpy.getLong(ConstantPreference.cacheExpiry), Date().time)
            assertEquals(spyList[0].builtBy?.get(0)?.repoId, 1)
        }
    }

    @Test
    fun testFetchingRepositories() {
        val spyList = getSpyList()
        Mockito.`when`(repositoryDao.getAll()).thenReturn(spyList)
        localRepositoryManager.fetchRepositories {
            assertEquals(it.size, spyList.size)
        }
    }

    private val tempData:String = "[\n" +
            "{\n" +
            "author: \"testerSunshine\",\n" +
            "name: \"12306\",\n" +
            "avatar: \"https://github.com/testerSunshine.png\",\n" +
            "url: \"https://github.com/testerSunshine/12306\",\n" +
            "description: \"12306智能刷票，订票\",\n" +
            "language: \"Python\",\n" +
            "languageColor: \"#3572A5\",\n" +
            "stars: 23584,\n" +
            "forks: 7429,\n" +
            "currentPeriodStars: 1968,\n" +
            "builtBy: [\n" +
            "{\n" +
            "username: \"testerSunshine\",\n" +
            "href: \"https://github.com/testerSunshine\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/20162049\"\n" +
            "},\n" +
            "{\n" +
            "username: \"MonsterTan\",\n" +
            "href: \"https://github.com/MonsterTan\",\n" +
            "avatar: \"https://avatars1.githubusercontent.com/u/22610809\"\n" +
            "},\n" +
            "{\n" +
            "username: \"cclauss\",\n" +
            "href: \"https://github.com/cclauss\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/3709715\"\n" +
            "},\n" +
            "{\n" +
            "username: \"stormeyes\",\n" +
            "href: \"https://github.com/stormeyes\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/5072174\"\n" +
            "},\n" +
            "{\n" +
            "username: \"BlancRay\",\n" +
            "href: \"https://github.com/BlancRay\",\n" +
            "avatar: \"https://avatars1.githubusercontent.com/u/9410067\"\n" +
            "}\n" +
            "]\n" +
            "},\n" +
            "{\n" +
            "author: \"agalwood\",\n" +
            "name: \"Motrix\",\n" +
            "avatar: \"https://github.com/agalwood.png\",\n" +
            "url: \"https://github.com/agalwood/Motrix\",\n" +
            "description: \"A full-featured download manager.\",\n" +
            "language: \"JavaScript\",\n" +
            "languageColor: \"#f1e05a\",\n" +
            "stars: 15558,\n" +
            "forks: 1712,\n" +
            "currentPeriodStars: 256,\n" +
            "builtBy: [\n" +
            "{\n" +
            "username: \"agalwood\",\n" +
            "href: \"https://github.com/agalwood\",\n" +
            "avatar: \"https://avatars1.githubusercontent.com/u/1032175\"\n" +
            "},\n" +
            "{\n" +
            "username: \"bladeaweb\",\n" +
            "href: \"https://github.com/bladeaweb\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/14051753\"\n" +
            "},\n" +
            "{\n" +
            "username: \"kant\",\n" +
            "href: \"https://github.com/kant\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/32717\"\n" +
            "},\n" +
            "{\n" +
            "username: \"mesquka\",\n" +
            "href: \"https://github.com/mesquka\",\n" +
            "avatar: \"https://avatars3.githubusercontent.com/u/3370615\"\n" +
            "},\n" +
            "{\n" +
            "username: \"Raistlin916\",\n" +
            "href: \"https://github.com/Raistlin916\",\n" +
            "avatar: \"https://avatars0.githubusercontent.com/u/2560402\"\n" +
            "}\n" +
            "]\n" +
            "}\n" +
            "]"

    private fun getSpyList(): List<Repository> {
        return Gson().fromJson(tempData, Array<Repository>::class.java).toList()
    }

}