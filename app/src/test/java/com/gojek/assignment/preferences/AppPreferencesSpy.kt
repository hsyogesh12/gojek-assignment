package com.gojek.assignment.preferences

class AppPreferencesSpy : AppPreferences {

  private val keyValues = mutableMapOf<String, Any?>()

  override fun getBoolean(key: String, defaultValue: Boolean): Boolean {
    return keyValues[key] as Boolean
  }

  override fun getString(key: String, defaultValue: String): String? {
    return keyValues[key] as String
  }

  override fun getLong(key: String, defaultValue: Long): Long {
    return keyValues[key] as Long
  }

  override fun getInt(key: String, defaultValue: Int): Int {
    return keyValues[key] as Int
  }

  override fun getFloat(key: String, defaultValue: Float): Float {
    return keyValues[key] as Float
  }

  override fun set(key: String, value: Any?) {
    keyValues[key] = value
  }

  fun clear() {
    keyValues.clear()
  }
}