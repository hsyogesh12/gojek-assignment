package com.gojek.assignment.viewmodel

import com.gojek.assignment.local.LocalRepositoryManagerImpl
import com.gojek.assignment.network.RemoteRepositoryMangerImpl
import com.gojek.assignment.room.entity.Repository
import com.gojek.assignment.utils.AppUtil
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {


    private lateinit var mainViewModel: MainViewModel

    @Mock
    private lateinit var localRepositoryManager: LocalRepositoryManagerImpl

    @Mock
    private lateinit var remoteRepositoryManager: RemoteRepositoryMangerImpl

    @Mock
    private lateinit var appUtil: AppUtil

    @Mock
    private lateinit var callback: (List<Repository>) -> Unit


    @Before
    fun setUp() {
        mainViewModel = MainViewModel(remoteRepositoryManager, appUtil, localRepositoryManager)
    }

    @Test
    fun testGetRepositories() {
        Mockito.`when`(appUtil.isCacheExpired()).thenReturn(true)
        mainViewModel.getRepositories(callback)
        Mockito.verify(remoteRepositoryManager).fetchRepositories(callback)

        Mockito.`when`(appUtil.isCacheExpired()).thenReturn(false)
        mainViewModel.getRepositories(callback)
        Mockito.verify(localRepositoryManager).fetchRepositories(callback)
    }

    @Test
    fun testGettingRepositoriesFromRemote() {
        mainViewModel.getRepositoriesFromRemote {
            Assert.assertEquals(0, it.size)
        }

    }


}