/*
package com.gojek.assignment.data

import com.gojek.assignment.room.entity.Repository
import com.google.gson.Gson
import io.reactivex.subscribers.TestSubscriber
import org.junit.Before
import org.junit.Test
import java.util.ArrayList
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RemoteDataSourceTest {

    private lateinit var mResultList: MutableList<Repository>
    private lateinit var mMockWebServer: MockWebServer
    private lateinit var mSubscriber: TestSubscriber<List<Repository>>

    @Before
    fun setUp() {
        val repository = Repository("Medium","","","","","","",1,1,1)
        val repository1 = Repository("Google","","","","","","",1,1,1)
        mResultList = ArrayList()
        mResultList.add(repository)
        mResultList.add(repository1)

        mMockWebServer = MockWebServer()
        mSubscriber = TestSubscriber()
    }

    @Test
    fun serverCallWithResponse() {
        //Given
        val url = "dfdf/"
        mMockWebServer.enqueue(MockResponse().setBody(Gson().toJson(mResultList)))
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(mMockWebServer.url(url))
            .build()
        val remoteDataSource = RemoteDataSource(retrofit)

        //When
        remoteDataSource.getRepositories().subscribe(mSubscriber)

        //Then
        mSubscriber.assertNoErrors()
        mSubscriber.assertComplete()
    }

    @Test
    fun severCallWithSuccessful() {
        //Given
        val url = "https://github-trending-api.now.sh"
        mMockWebServer.enqueue(MockResponse().setBody(Gson().toJson(mResultList)))
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(mMockWebServer.url(url))
            .build()
        val remoteDataSource = RemoteDataSource(retrofit)

        //When
        remoteDataSource.getRepositories().subscribe(mSubscriber)

        //Then
        mSubscriber.assertNoErrors()
        mSubscriber.assertComplete()
    }

}*/
