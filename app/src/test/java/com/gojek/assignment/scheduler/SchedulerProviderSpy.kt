package com.gojek.assignment.scheduler

import com.gojek.assignment.schedulers.SchedulerProvider

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class SchedulerProviderSpy : SchedulerProvider {


    override fun io(): Scheduler {
        return Schedulers.single()
    }

    override fun ui(): Scheduler {
        return Schedulers.single()
    }
}
